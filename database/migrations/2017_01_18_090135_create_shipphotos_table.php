<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ship_photos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ship_id')->unsigned()->default(1);
            $table->string('image_url');
            $table->string('author');
            $table->string('location');
            $table->string('date');
            $table->timestamps();
//            $table->foreign('ship_id')->references('id')->on('ships');
//            $table->foreign('ship_id')->references('id')->on('ships')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ship_photos');
    }
}
