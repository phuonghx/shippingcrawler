<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ship extends Model
{
    protected $fillable = [
        'imo', 'name', 'former_name', 'type', 'flag', 'flag_image', 'grt', 'dwt', 'loa', 'bm', 'speed', 'teu', 'pax', 'mmsi', 'official_number', 'callsign', 'owners', 'management_company', 'builder', 'yard', 'sales_and_transfers', 'accidents', 'broken_up', 'date_broken_up', 'las_up_date', 'url_id', 'status'
    ];

//    public function shipphoto() {
//        return $this->hasMany('App\Ship');
//    }
}
