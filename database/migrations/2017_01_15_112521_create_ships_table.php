<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ships', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url_id');
            $table->boolean('status');
            $table->integer('imo');
            $table->string('name');
            $table->string('former_name');
            $table->string('type');
            $table->string('flag');
            $table->string('flag_image');
            $table->string('grt');
            $table->string('dwt');
            $table->string('loa');
            $table->string('bm');
            $table->string('speed');
            $table->string('teu');
            $table->string('pax');
            $table->string('mmsi');
            $table->string('official_number');
            $table->string('callsign');
            $table->string('owners');
            $table->string('management_company');
            $table->string('builder');
            $table->string('yard');
            $table->string('sales_and_transfers');
            $table->string('accidents');
            $table->string('broken_up');
            $table->string('date_broken_up');
            $table->string('las_up_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ships');
    }
}
