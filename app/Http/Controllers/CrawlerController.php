<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;
use App\Ship;
use App\ShipPhoto;
use DB;

include_once('simple_html_dom.php');
//include_once ('php-image-crawler/phpQuery-onefile.php');

class CrawlerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // $x = 0;
        // while($x < 100) {
        $ship = new Ship();
        $client = new Client();

     //  Hackery to allow HTTPS
        $guzzleclient = new \GuzzleHttp\Client([
            'timeout' => 120,
            'verify' => false,
        ]);

        // Create DOM from URL or file
        // 
        $row = Ship::all()->count();

        // dd($row);
        $id = $row + 1;
//        $id =  13;
        // dd($id);
        $html = file_get_html("http://www.shippingdatabase.com/ship.php?shipid=$id");
// dd($html);
// http://www.shippingdatabase.com/ship.php?shipid=200785
// http://www.shippingdatabase.com/ship.php?shipid=200585
// http://www.shippingdatabase.com/ship.php?shipid=200385
// http://www.shippingdatabase.com/ship.php?shipid=200285

        // // Find all images
        // foreach ($html->find('img') as $element) {
        //     echo $element->src . '<br>';
        // }

        // // Find all links
        // foreach ($html->find('a') as $element) {
        //     echo $element->href . '<br>';
        // }
        $elements = array();
        foreach ($html->find('.boatdetails td') as $element) {
            
            // array_push($elements,self::preg_trim($element->text()));
            array_push($elements,trim($element->text()));
        }
        if(count($elements) == 0) {
            $ship['url_id'] = $id;
            $ship['status'] = 0;

            $ship->save();
            return view('crawler.index', compact('elements'));
        }
        // 
        // 
        // $element = $html->find('.boatdetails td');
        // echo '<pre>';
        // var_dump($elements);
        // echo '</pre>';
        // dd($elements);
        // $a =  $elements[3]->text();
        // echo $a;
        $this->image($id);

        $ship['url_id'] = $id;
        $ship['status'] = 1;
        $ship['imo'] = $elements[1];
        $ship['name'] = $elements[4];
        $ship['former_name'] = $elements[9];
        $ship['type'] = $elements[11];
        $ship['flag'] = $elements[13];
        $ship['flag_image'] = 'a';
        $ship['grt'] = $elements[16];
        $ship['dwt'] = $elements[18];
        $ship['loa'] = $elements[20];
        $ship['bm'] = $elements[22];
        $ship['speed'] = $elements[24];
        $ship['teu'] = $elements[26];
        $ship['pax'] = $elements[28];
        $ship['mmsi'] = $elements[30];
        $ship['official_number'] = $elements[32];
        $ship['callsign'] = $elements[34];
        $ship['owners'] = $elements[37];
        $ship['management_company'] = $elements[39];
        $ship['builder'] = $elements[41];
        $ship['yard'] = $elements[43];
        $ship['sales_and_transfers'] = $elements[46];
        $ship['accidents'] = $elements[48];
        $ship['broken_up'] = $elements[50];
        $ship['date_broken_up'] = $elements[52];
        $ship['las_up_date'] = $elements[54];
        $ship->save();
        // $x++;
        // }

        return view('crawler.index', compact('elements', 'id'));
    }

    public function image($ship_id) {
        $client = new Client();
        //  Hackery to allow HTTPS
        $guzzleclient = new \GuzzleHttp\Client([
            'timeout' => 120,
            'verify' => false,
        ]);
//        $ship_id = 200285;
//        $ship_id = $this->$ship_id;
        $html = file_get_html("http://www.shippingdatabase.com/ship.php?shipid=$ship_id");
        // // Find all images
        $images[] = array();
         foreach ($html->find('.thickbox') as $element) {
//             dd($element->href);
             array_push($images,trim($element->href));
//             echo $element->text() . '<br>';
         }
//         dd($images);
//        dd(count($images));
//        dd($images[1]);
        // // Find description
        $description = array();
        foreach ($html->find('#rightcol table tbody tr td') as $e) {
            array_push($description, $e->text());
//             dd($e);
        }
//        dd($description);
        //Get the file

//        mkdir("/images/$ship_id", 0777, true);
        $count = 0;
        foreach ($images as $image) {
            if ($image != null) {
//                Create new ship photo
                $shipphoto = new ShipPhoto();


//                DB::table('ship_photos')->insert([
//                    'ship_id' => $ship_id,
//                    'image_url' => $ship_id . '_' . $count . '.jpg',
//                    'author' => $description[1 + ($count - 1) * 4],
//                    "location" =>  $description[2 + ($count - 1) * 4],
//                    "date" => $description[3 + ($count - 1) * 4],
//                ]);


                $url = 'http://www.shippingdatabase.com';
                $url = $url . $image;
                $content = file_get_contents($url);
//                dd($_SERVER['SERVER_NAME']);
//                dd(__DIR__);
                //Store in the filesystem.
//                $fp = fopen("/images/$ship_id/"  . $ship_id . '_' . $count . '.jpg', "w");
                $fp = fopen(base_path() . '/public/images/' . $ship_id . '_' . $count . '.jpg', "w");
//                $fp = fopen($_SERVER['DOCUMENT_ROOT'] . 'images/' . $ship_id . '_' . $count . '.jpg', "w");
                fwrite($fp, $content);
                fclose($fp);
                chmod(base_path() . '/public/images/' . $ship_id . '_' . $count . '.jpg', 0777);
                $shipphoto['ship_id'] = $ship_id;
                $shipphoto['image_url'] = $ship_id . '_' . $count . '.jpg';
                $shipphoto['author'] = $description[1 + ($count - 1) * 4];
                $shipphoto['location'] = $description[2 + ($count - 1) * 4];
                $shipphoto['date'] = $description[3 + ($count - 1) * 4];



                $shipphoto->save();
            }
            $count++;
        }
//        chmod("/images/$ship_id", 0777);

//        return view('crawler.image', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function preg_trim($subject) {
        $regex = "/\s*(\.*)\s*/s";
        if (preg_match ($regex, $subject, $matches)) {
            $subject = $matches[1];
        }
        return $subject;
    }
}
