<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShipPhoto extends Model
{
    protected $fillable = [
        'ship_id', 'image_url', 'author', 'location', 'date'
    ];

    public function ship() {
        return $this->belongsTo('App\Ship');
    }
}
